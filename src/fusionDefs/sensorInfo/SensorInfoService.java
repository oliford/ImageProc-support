package fusionDefs.sensorInfo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;

/** Service to provide deciphering of CamModeInfo from various camera and spectrometer metdata */ 
public abstract class SensorInfoService {

	@FunctionalInterface
	public static interface MetaDataGetter {		
		public <T> T get(String name, Class<T> desiredType);
	};
	
	public static interface SensorParser{
		public SensorInfo attemptGet(MetaDataGetter src);
	}
	
	private static ArrayList<SensorParser> camParsers;
	
	static {
		camParsers = new ArrayList<SensorParser>();
		camParsers.add(new SensorInfo_AndorCMOS());
		camParsers.add(new SensorInfo_AndorV2());
		camParsers.add(new SensorInfo_PICam());
		camParsers.add(new SensorInfo_PCOEdge());
		camParsers.add(new SensorInfo_OceanOptics());
		camParsers.add(new SensorInfo_Sensicam());
	}
	
	
	/** Gets all camera mode information from a map of metadata, attempting all registered camera/sensor types.
	 * If the general 'cameraModeInfo' is already in the map, that is preferred.
	 * If not, and it is deciphered from specific camera metadata, then the generalised metadata will be written back
	 * 
	 * @param toMap If not null, standardised entries are added to this map 
	 */
	public static SensorInfo decipher(MetaDataGetter src) {
		SensorInfo info = null;
		
		//see if it's already got the generalised metadata
		try {
			return SensorInfo.fromMetadata(src, "/cameraModeInfo");			
		}catch(RuntimeException err) { 
			//err.printStackTrace();
		}
		
		//now try each registered camera type
		for(SensorParser parser : camParsers) {
			info = parser.attemptGet(src);
			if(info != null)
				break;
		}
		
		if(info == null)
			throw new RuntimeException("Unable to find camera mode information '/cameraModeInfo', or from any recognised camera/sensor metadata");
		
		//if we got the info from the camera, we also need to check if the metadata 
		//says this data was binned in the Software, which changes the baseline level
		SensorInfo_SoftwareROIsProcessor.check(src, info);
				
		return info;
		
	}
		
	/** Attempt to get the camera mode info of a given type of camera */
	protected abstract SensorInfo attemptGet(MetaDataGetter src);
	
	/** Look for a/b/c/.../{expectedKey}, and return the prefix 'a/b/c/...'
	 * or null if it wasn't found  */
	protected String findPrefix(Map<String, Object> map, String expectedKey, String expectedPrefix) {
		//look for expected prefix first (fast)
		if(map.containsKey(expectedPrefix + "/" + expectedKey))
			return expectedPrefix;
		
		//look without prefix (fast)
		if(map.containsKey(expectedKey))
			return "";
		if(map.containsKey("/" + expectedKey))
			return "/";
		
		//do full slow search
		for(String key : map.keySet()) {
			if(key.endsWith("/" + expectedKey)) {
				return key.substring(0, key.length() - (expectedKey.length() + 1));
			}
		}
		
		return null;//otherwise didn't find it		
	}
	
	/* Get a single object entry from the map, pulling it out of arrays if it's 
	 * got wrapped up in one for whatever reason 
	protected static Object getMetaObject(Map<String, Object> map, String key){
		key.replaceAll("^[\\\\s*][/*]", ""); //strip leading slashes and spaces		
		Object o = map.get(key);
		if(o == null)
			o = map.get("/" + key); //try with a leading /
		while(o != null && o.getClass().isArray()){
			o = Array.get(o, 0);
		}
		return o;
	}

	protected static Double getMetaDouble(Map<String, Object> map, String key) {
		Object o = getMetaObject(map, key);
		if(o == null)
			return null;
		if(o instanceof Double)
			return (Double)o;
		if(o instanceof Number)
			return ((Number)o).doubleValue();
		else 
			throw new RuntimeException("Unexpected type "+o.getClass()+" of entry "+key+" looking for a double");
	}
	
	protected static Integer getMetaInteger(Map<String, Object> map, String key) {
		Object o = getMetaObject(map, key);
		if(o == null)
			return null;
		if(o instanceof Integer)
			return (Integer)o;
		if(o instanceof Number)
			return ((Number)o).intValue();
		else 
			throw new RuntimeException("Unexpected type "+o.getClass()+" of entry "+key+" looking for a int");
	}
	
	protected static Long getMetaLong(Map<String, Object> map, String key) {
		Object o = getMetaObject(map, key);
		if(o == null)
			return null;
		if(o instanceof Integer)
			return (Long)o;
		if(o instanceof Number)
			return ((Number)o).longValue();
		else 
			throw new RuntimeException("Unexpected type "+o.getClass()+" of entry "+key+" looking for a long");
	}*/
}
