package fusionDefs.sensorInfo;

import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import fusionDefs.sensorInfo.SensorInfoService.SensorParser;
import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;

public class SensorInfo_PICam implements SensorParser {
	
	
	//definitions from SDK picam.h. Also in Java in ImageProc/driver-picam
    public static enum PicamReadoutControlMode {
    	UNKNOWN, FullFrame, FrameTransfer, Kinetics, 
    	SpectraKinetics, Interline, Dif, SeNsR, RollingShutter;	
    }


    public static enum PicamAdcQuality { UNKNOWN, LowNoise, HighCapacity, ElectronMultiplied, HighSpeed; }

    public static enum PicamAdcAnalogGain { UNKNOWN, Low, Medium, High; }

    public static enum PicamSensorType { UNKNOWN, Ccd, InGaAs, Cmos; }

    public static enum PicamCcdCharacteristicsMask { 
    	None(0x000), 
    	BackIlluminated(0x001), 
    	DeepDepleted(0x002), 
    	OpenElectrode(0x004), 
    	UVEnhanced(0x008),
    	ExcelonEnabled(0x010),
    	SecondaryMask(0x020), 
    	Multiport(0x040),
    	AdvancedInvertedMode(0x080), 
    	HighResistivity(0x100);
    	
    	public int mask;
    	PicamCcdCharacteristicsMask(int mask){ this.mask = mask; }
    }


	public static class SensorInfoPICam extends SensorInfo {
		public PicamReadoutControlMode readoutControlMode;
		public PicamAdcQuality adcQuality;
		public PicamAdcAnalogGain adcAnalogGain;
		public PicamSensorType sensorType;
		public int characteristicsMask;
		
		public double adcEMGain;
		public double adcSpeed;
		public int adcBitDepth;
		public double verticalShiftRate;
		public double sensorTemperatureReading; 
		
		private String getCharacteristicsString() {
			String ret = "";
			for(PicamCcdCharacteristicsMask c : PicamCcdCharacteristicsMask.values()) {
				if((characteristicsMask & c.mask) > 0)
					ret += c.name() + " "; 
			}
			return ret;
		}
	}
	
	@Override
	public SensorInfo attemptGet(MetaDataGetter src) {		
		//try to find the camera metadata		
		
		// Princeton ProEM cameras should always be under 'PICam'.
		String readoutMode;
		try { 
			Integer rcMode = src.get("PICam/config/ReadoutControlMode", Integer.class);
			if(rcMode == null)
				return null;
		}catch(RuntimeException err) {			
			return null; //No AndorCam metadata, ignore
		}
		
		SensorInfoPICam camInfo = new SensorInfoPICam();
		
		camInfo.adcEMGain = src.get("PICam/config/AdcEMGain", Double.class);
		camInfo.adcSpeed = src.get("PICam/config/AdcSpeed", Double.class);
		camInfo.adcBitDepth = src.get("PICam/config/AdcBitDepth", Integer.class);
		camInfo.verticalShiftRate = src.get("PICam/config/VerticalShiftRate", Double.class);
		camInfo.sensorTemperatureReading = src.get("PICam/config/SensorTemperatureReading", Double.class);
		
		int v = src.get("PICam/config/ReadoutControlMode", Integer.class);
        camInfo.readoutControlMode = PicamReadoutControlMode.values()[v];

        v = src.get("PICam/config/AdcQuality", Integer.class);
        camInfo.adcQuality = PicamAdcQuality.values()[v];

        v = src.get("PICam/config/AdcAnalogGain", Integer.class);
        camInfo.adcAnalogGain = PicamAdcAnalogGain.values()[v];

        v = src.get("PICam/config/SensorType", Integer.class);
        camInfo.sensorType = PicamSensorType.values()[v];

        camInfo.characteristicsMask = src.get("PICam/config/CcdCharacteristics", Integer.class);
        
	    camInfo.exposureTime = src.get("PICam/config/ExposureTime", Double.class) * 1e-3;
	    camInfo.frameTime = 1.0 / src.get("PICam/config/FrameRateCalculation", Double.class);
	    //we don't seem to have any more info in the metadata for the camera type/name
	    camInfo.cameraName = "PICam " + camInfo.sensorType + " " + camInfo.getCharacteristicsString();
	    camInfo.modeString = camInfo.cameraName + " " + camInfo.readoutControlMode + ", ADC=" 
	    												+ camInfo.adcQuality + ","
	    												+ camInfo.adcAnalogGain  + "," 
	    												+ camInfo.adcSpeed + "MHz, x" 
	    												+ camInfo.adcEMGain + "EM";

	    camInfo.photoelectronsPerADCU = Double.NaN;
    	camInfo.readNoise = Double.NaN;

	    if(camInfo.readoutControlMode == PicamReadoutControlMode.FrameTransfer ||
	    			camInfo.readoutControlMode == PicamReadoutControlMode.FullFrame) {
	    
	    		if(camInfo.adcQuality == PicamAdcQuality.ElectronMultiplied &&
	    				camInfo.adcAnalogGain == PicamAdcAnalogGain.High) {
		
			    	// Determined from lots of fits to the noise for AUG1 and AUG2 
			    	camInfo.photoelectronsPerADCU = 0.9 / camInfo.adcEMGain;
			    	camInfo.readNoise = 40; // Doesn't depend on H/W binning, because readout is once for whole bin

	    		}else if(camInfo.adcQuality == PicamAdcQuality.LowNoise &&
	    				camInfo.adcAnalogGain == PicamAdcAnalogGain.High) {
	
			    	camInfo.photoelectronsPerADCU = 0.9; // Determined from PI_CCD_07_1 during Zeff calibration
			    	camInfo.readNoise = 15.5;
	    		}
	    }

	    if(Double.isNaN(camInfo.photoelectronsPerADCU))
	    	Logger.getLogger(this.getClass().getName()).warning("No photoelectron count or noise information for this PICam mode ("+camInfo.modeString+")");

	    //camera AOI (which is 1-based!!)
	    camInfo.sensorPixelOfImageLeft = 0;/// FIXME! src.get("/AndorCam/config/AOILeft", Integer.class) - 1;
	  	camInfo.sensorPixelOfImageTop = 0;/// FIXME! src.get("/AndorCam/config/AOITop", Integer.class) - 1;
	  	//String aoiBinning = src.get("/AndorCam/config/AOIBinning", String.class);
	  	//String parts[] = aoiBinning.split("x");	  		
	  	//camInfo.sensorBinningX = Integer.parseInt(parts[0]);
	  	//camInfo.sensorBinningY = Integer.parseInt(parts[1]);
	  	camInfo.sensorBinningX = 1; //FIXME!
	  	camInfo.sensorBinningY = 1;
	  		
	  	camInfo.transposedImage = false; //camera doesn't support it	
	  		

		return camInfo;
		
	}


}
