package fusionDefs.sensorInfo;

import java.util.HashMap;
import java.util.Map;

import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;

/** Stores, deciphers and converts information about CCD and CMOS recording modes for a set of images
 * 
 * Used to determine zero level and photo-electron counts.
 * Some general terms:
 *   - Photoelectrons / 'counts' - Whatever is being counted and determines the counting noise
 *   - ADCU - 'ADC' units or 'value' - whatever number is given by the camera. Not really a 'count', although they
 *      documentation often call them this.
 *  
 * @author oliford
 *
 */
public class SensorInfo {	

	/** Some short identifier of the camera. Typically manufacturer and model*/
	public String cameraName;
	
	/** Zero level - value in image when there is no light. No calibrated but what is given by
	 * the driver/camera. [ADCU] */
	public double zeroLevel;
	
	/** Length of time light was collected for. [s] */
	public double exposureTime;
	
	/** Length of time between frames (same as 1 / frameRate[Hz]) [s] */
	public double frameTime; 
	
	/** A human readable description of the most pertinent camera mode information. */
	public String modeString;
	
	/** The value in each pixel per photo electron counted [ADCU]. */
	public double photoelectronsPerADCU;
	
	/** The noise level of the reading in this mode [ADCU] */
	public double readNoise;
	
	/** First pixel on the real sensor that contributes to the left most pixel in the images.
	 * If the image was transposed at some point, this will be the sensor Y value.
	 * This will include hardware as well as any software binning */
	public int sensorPixelOfImageLeft;
	
	/** First pixel on the real sensor that contributes to the top most pixel in the images
	 *  If the image was transposed at some point, this will be the sensor Y value. 
	 *  * This will include hardware as well as any software binning */
	public int sensorPixelOfImageTop;
	
	/** Number of sensor pixels in X direction that contribute to a image pixel. 
	 * If the image was transposed, this relates to binning in the y direction of the image. */
	public int sensorBinningX;
	
	/** Number of sensor pixels in Y direction that contribute to a image pixel 
	 * If the image was transposed, this relates to binning in the x direction of the image. */
	public int sensorBinningY;
	
	/** Image from the sensor was actually or effectively transposed to produce the image */ 
	public boolean transposedImage;
	
	/** offsets only from software binning.
	 * Value of first pixel in image returned from camera that contributes to first pixel
	 * of image processed by SoftwareROIProcessor.
	 */
	public int softwareROIOffsetX;
	public int softwareROIOffsetY;
	public int softwareROIBinningX;
	public int softwareROIBinningY;
	
	protected SensorInfo() {
		cameraName = "UNKNOWN";
		zeroLevel = Double.NaN;
		exposureTime = Double.NaN;
		frameTime = Double.NaN;
		modeString = "UNKNOWN";
		photoelectronsPerADCU = Double.NaN;
		readNoise = Double.NaN;
		
		sensorPixelOfImageLeft = -1;
		sensorPixelOfImageTop = -1;
		sensorBinningX = -1;
		sensorBinningY = -1;
		transposedImage = false;

		softwareROIOffsetX = -1;
		softwareROIOffsetY = -1;
		softwareROIBinningX = -1;
		softwareROIBinningY = -1;
	}

	/** Reads camera mode information from a metadata map */ 
	public static SensorInfo fromMetadata(MetaDataGetter src, String prefix) {		

		SensorInfo camInfo = new SensorInfo();
		camInfo.cameraName = (String) src.get(prefix + "/cameraName", String.class);
		camInfo.zeroLevel = (Double) src.get(prefix + "/zeroLevel", Double.class);
		camInfo.exposureTime = (Double)src.get(prefix + "/exposureTime", Double.class);
		camInfo.frameTime = (Double) src.get(prefix + "/frameTime", Double.class);
		camInfo.modeString = (String) src.get(prefix + "/modeString", String.class);
		camInfo.photoelectronsPerADCU = (Double) src.get(prefix + "/photoelectronsPerADCU", Double.class);
		camInfo.readNoise = (Double) src.get(prefix + "/readNoise", Double.class);
		
		camInfo.sensorPixelOfImageLeft = (Integer) src.get(prefix + "/sensorPixelOfImageLeft", Integer.class);
		camInfo.sensorPixelOfImageTop = (Integer) src.get(prefix + "/sensorPixelOfImageTop", Integer.class);
		camInfo.sensorBinningX = (Integer) src.get(prefix + "/sensorBinningX", Integer.class);
		camInfo.sensorBinningY = (Integer) src.get(prefix + "/sensorBinningY", Integer.class);
		camInfo.transposedImage = (Boolean) src.get(prefix + "/transposedImage", Boolean.class);

		camInfo.softwareROIOffsetX = (Integer) src.get(prefix + "/softwareROIOffsetX", Integer.class);
		camInfo.softwareROIOffsetY = (Integer) src.get(prefix + "/softwareROIOffsetY", Integer.class);
		camInfo.softwareROIBinningX = (Integer) src.get(prefix + "/softwareROIBinningX", Integer.class);
		camInfo.softwareROIBinningY = (Integer) src.get(prefix + "/softwareROIBinningY", Integer.class);
		
		return camInfo;		
	}
	
	public HashMap<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put(prefix + "/cameraName", cameraName);
		map.put(prefix + "/zeroLevel", zeroLevel);
		map.put(prefix + "/exposureTime", exposureTime);
		map.put(prefix + "/frameTime", frameTime);
		map.put(prefix + "/modeString", modeString);
		map.put(prefix + "/photoelectronsPerADCU", photoelectronsPerADCU);
		map.put(prefix + "/readNoise", readNoise);
		
		map.put(prefix + "/sensorPixelOfImageLeft", sensorPixelOfImageLeft);
		map.put(prefix + "/sensorPixelOfImageTop", sensorPixelOfImageTop);
		map.put(prefix + "/sensorBinningX", sensorBinningX);
		map.put(prefix + "/sensorBinningY", sensorBinningY);
		map.put(prefix + "/transposedImage", transposedImage);

		map.put(prefix + "/softwareROIOffsetX", softwareROIOffsetX);
		map.put(prefix + "/softwareROIOffsetY", softwareROIOffsetY);
		map.put(prefix + "/softwareROIBinningX", softwareROIBinningX);
		map.put(prefix + "/softwareROIBinningY", softwareROIBinningY);
		return map;
	}
	
}
