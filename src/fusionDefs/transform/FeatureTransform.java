package fusionDefs.transform;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;

import mds.GMDSFetcher;
import signals.gmds.GMDSSignal;
import descriptors.gmds.GMDSSignalDesc;

/** Hold info about the 3D - image - beam coordinate transform.
 * 
 * The primary 2D-2D transform is from physical space coordinates at some plane (i.e. the CCD) (in m) to lat/lon angle away from 
 * the view position. 
 * 
 * The physical CCD coordinates are generally relative to the center of the 'full frame' CCD.
 * The size of the CCD and the actual coverage of the CCD by the image depends on camera and it's settings (e.g. ROI) and is handled in CCDImageTransform
 * 
 * It's used by IMSEProc, minerva-imse, and the IMSE ray tracing apps.
 * 
 * Conversion to 'beam plane' (R,Z) coordinates are not done here.
 */
public abstract class FeatureTransform {
	
	/** Range info for lat/lon (defines the LL-->XY interpolation grid) */	
	protected double minLon, maxLon, minLat, maxLat;

	protected ArrayList<TransformPoint> points = new ArrayList<TransformPoint>();
	
	public FeatureTransform(){
		
	}
		
	public ArrayList<TransformPoint> getPoints(){ return points; }	
	public TransformPoint getPointByName(String name){
		synchronized (points) {
			for(TransformPoint p : points){
				if(name.equals(p.name))
					return p;					
			}
		}
		return null;
	}
	
	public double[] getViewPosition(){
		synchronized (points) {		
			for(TransformPoint p : points){
				if(p.mode == TransformPoint.MODE_VIEW)
					return p.getPosXYZ();
			}
		}
		return null;
	}
	
	public double[] getTargetPosition(){
		synchronized (points) {		
			for(TransformPoint p : points){
				if(p.mode == TransformPoint.MODE_TARGET)
					return p.getPosXYZ();
			}
		}
		return null;
	}

	public void saveToSignal(GMDSFetcher gmds, String experiment, int pulse){
		savePoints(gmds, experiment, pulse);
	}
		
	public void savePoints(GMDSFetcher gmds, String experiment, int pulse){
		String pointNames[] = new String[points.size()];
		double pointData[][] = new double[points.size()][];
		
		int i=0;
		synchronized (points) {
			for(TransformPoint p : points){
				pointNames[i] = p.name;
				pointData[i] = p.toArray();
				i++;
			}
		}
			
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "Transform/pointNames");
		GMDSSignal sig = new GMDSSignal(sigDesc, pointNames);
		gmds.writeToCache(sig);

		sigDesc = new GMDSSignalDesc(pulse, experiment, "Transform/pointData");
		sig = new GMDSSignal(sigDesc, pointData);
		gmds.writeToCache(sig);
	
	}
	
	/** Construct from metaData signals */
	public FeatureTransform(GMDSFetcher gmds, String experiment, int pulse) {
		loadPoints(gmds, experiment, pulse);				
	}

	/** Construct from metaData  */
	public FeatureTransform(Map<String, Object> metaData) {
		loadPoints(metaData);				
	}
	
	public void loadPoints(GMDSFetcher gmds, String experiment, int pulse){
		
		//try reading from this specific pulse first
		String pointNames[] = null;
		double pointData[][] = null;
		
		GMDSSignal sig;
		GMDSSignalDesc sigDesc;
			
		sigDesc = new GMDSSignalDesc(pulse, experiment, "Transform/pointData");		
		sig = (GMDSSignal)gmds.getSig(sigDesc);
		pointData = (double[][])sig.get2DData();
			
		try{
			sigDesc = new GMDSSignalDesc(pulse, experiment, "Transform/pointNames");		
			sig = (GMDSSignal)gmds.getSig(sigDesc);
			pointNames = (String[])sig.get1DData();
				
		}catch(RuntimeException e){
			//hack for HDF5 string reading problem = regenerate now and save as netCDF from now on
			DecimalFormat fmt = new DecimalFormat("##");
			fmt.setMinimumIntegerDigits(2);
			pointNames = new String[pointData.length];
			for(int i=0; i <pointNames.length; i++){
				pointNames[i] = "point_" + fmt.format(i+1);
			}				
		}
		
		loadPoints(pointNames, pointData);
	}
		
	public void loadPoints(Map<String, Object> metaData){
		String pointNames[] = (String[]) metaData.get("/Transform/pointNames");
		double pointData[][] = (double[][]) metaData.get("/Transform/pointData");
		
		loadPoints(pointNames, pointData);
	}
		
	private void loadPoints(String pointNames[], double pointData[][]){
		synchronized (points) {
			points.clear();
			
			//some hackery to detect old-style saving of the imgX,imgY coords
			// oldest: pixels on image, integers
			// middle: 0.0 - 1.0 across image
			// newest: physical position on CCD
			boolean coordsArePhysical = false;
			boolean coordsArePixels = true;
			for(int i=0; i < pointNames.length; i++){
				double x = pointData[i][0], y =  pointData[i][1]; //TransformPoint.SIG_3D_X, TransformPoint.SIG_3D_Y
				
				if(x < 0 || y < 0)
					coordsArePhysical = true;
				
				if((x % 1.0) != 0.0 || (y % 1.0) != 0.0) // non-integer means it's not pixels
					coordsArePixels = false;
						
			}
			if(!coordsArePhysical){
				System.err.println("WARNING: FeatureTransform: Transform data looks like old image coordinates, converting.");
				
				for(int i=0; i < pointNames.length; i++){
					//assume the old default sensicam full ROI 2x2 binning, 9.9um pixels
					
					if(coordsArePixels){ //data was in whole pixels
						pointData[i][0] /= 320; //assume 320x240, although it might now be
						pointData[i][1] /= 240;
					}
					
					//now in 0.0-1.0 range
					pointData[i][0] = (pointData[i][0] - 0.5) * 640 * 9.9e-6;  
					pointData[i][1] = (pointData[i][1] - 0.5) * 480 * 9.9e-6;
				}
			}
			
			for(int i=0; i < pointNames.length; i++){
				points.add(new TransformPoint(pointNames[i], pointData[i]));
			}
		}
	}
	
		
	public abstract double[] latlonToXY(double lat, double Lon);
	public abstract double[] xyToLatLon(double x, double y);
	public abstract boolean isValid();



	public final double getMinLon() { return minLon; }
	public final double getMaxLon() { return maxLon; }
	public final double getMinLat() { return minLat; }
	public final double getMaxLat() { return maxLat; }
	
	
}
