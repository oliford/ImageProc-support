package fusionDefs.sensorInfo;

import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;

/** Modifies background level according to software binning */
public abstract class SensorInfo_SoftwareROIsProcessor {

	public static void check(MetaDataGetter src, SensorInfo info) {
		try { 
			Object o = src.get("SoftwareROIs/Rois_arrays/names", String[].class);
			if(o == null)
				return; //no Software ROIs
		}catch(RuntimeException err) {
			return; //No AndorCam metadata, ignore
		}
		
		int[] xBin = (int[])src.get("SoftwareROIs/Rois_arrays/x_binning", int[].class);
		int[] yBin = (int[])src.get("SoftwareROIs/Rois_arrays/y_binning", int[].class);
		int[] x0 = (int[])src.get("SoftwareROIs/Rois_arrays/x", int[].class);
		int[] y0 = (int[])src.get("SoftwareROIs/Rois_arrays/y", int[].class);

		boolean softTranspose = (Boolean)src.get("SoftwareROIs/Rois_arrays/transposeOutput", Boolean.class);

		info.softwareROIOffsetX = x0[0];
		info.softwareROIOffsetY = y0[0];
		
		//we overwrite the camera's AOI information here because the SoftwareROIProcessor
		//config is given relative to the sensor, not relative to the unbinned recorded image. 
		info.sensorPixelOfImageLeft = x0[0];
		info.sensorPixelOfImageTop = y0[0];
		
		if(softTranspose) {
			//all the y0's should be the same
			for(int i=1; i < y0.length; i++)
				if(y0[i] != y0[0])
					throw new RuntimeException("CamInfo only supports software ROIs where all tracks have the same x offset (which is y, because the bins are transposed here)");
		}else {
			//all the x0's should be the same
			for(int i=1; i < x0.length; i++)
				if(x0[i] != x0[0])
					throw new RuntimeException("CamInfo only supports software ROIs where all tracks have the same x offset.");
			
		}
		
		info.transposedImage ^= softTranspose;
		
		//software binning applied additionally to hardware binning of camera
		info.sensorBinningX *= xBin[0]; //binning talks about the original sensor direction
		info.sensorBinningY *= yBin[0];
		info.softwareROIBinningX = xBin[0];
		info.softwareROIBinningY = yBin[0];
		
		int nBin = xBin[0] * yBin[0];
		
		//check all channels are the same
		for(int i=0; i < xBin.length; i++) {
			if((xBin[i] * yBin[i]) != nBin)
				throw new RuntimeException("Some channels have different binning. This is not supported as the background level will be wrong");
		}
		
		info.zeroLevel *= nBin;
		

	}
	
}
