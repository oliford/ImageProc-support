package fusionDefs.transform;

import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

import descriptors.gmds.GMDSSignalDesc;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.CubicInterpolation2D;
import algorithmrepository.Mat;
import seed.digeom.Function;
import seed.digeom.FunctionND;
import seed.digeom.IDomain;
import seed.digeom.RectangularDomain;
import seed.optimization.HookeAndJeeves;
import signals.gmds.GMDSSignal;
import mds.GMDSFetcher;
import net.jafama.FastMath;

/** Linear and cubic version of FeatureTransform */
public class FeatureTransformCubic extends FeatureTransform {

	//cant be member variable, breaks serialisation
	private Logger logr() { return Logger.getLogger(this.getClass().getSimpleName()); }
	
	/** Axes of the cubic interpolators */
	private double pLon[], pLat[];
	
	/** Interpolators for the cubic transform. These give the values X and Y on a grid in LL */
	private CubicInterpolation2D interpX, interpY;

	/** Transform matrix of linear version */
	private double linXYtoLL[][], linLLtoXY[][];

	
	public FeatureTransformCubic() {
		super();
	}
	
	public FeatureTransformCubic(GMDSFetcher gmds, String experiment, int pulse) {
		super(gmds, experiment, pulse);
		
		loadLinear(gmds, experiment, pulse);
		loadKnots(gmds, experiment, pulse);
	}
	
	public FeatureTransformCubic(Map<String, Object> metaData) {
		super(metaData);
		
		loadLinear(metaData);
		loadKnots(metaData);
	}
	
	public void saveToSignal(GMDSFetcher gmds, String experiment, int pulse){
		super.saveToSignal(gmds, experiment, pulse);
		if(isLinearValid()) saveLinear(gmds, experiment, pulse);
		if(isCubicValid()) saveKnots(gmds, experiment, pulse);
	}
	
	public void loadLinear(GMDSFetcher gmds, String experiment, int pulse){
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "Transform/linearXYtoLL");		
		GMDSSignal sig = (GMDSSignal)gmds.getSig(sigDesc);
		linXYtoLL = (double[][])sig.getData();

		linLLtoXY = Mat.inv3x3(linXYtoLL);		
	}

	public void loadKnots(GMDSFetcher gmds, String experiment, int pulse){
		pLon = (double[])((GMDSSignal)gmds.getSig(new GMDSSignalDesc(pulse, experiment, "Transform/cubicKnotsLon"))).getData();
		pLat = (double[])((GMDSSignal)gmds.getSig(new GMDSSignalDesc(pulse, experiment, "Transform/cubicKnotsLat"))).getData();
		
		double kX[][] = (double[][])((GMDSSignal)gmds.getSig(new GMDSSignalDesc(pulse, experiment, "Transform/cubicKnotsX"))).getData();
		double kY[][] = (double[][])((GMDSSignal)gmds.getSig(new GMDSSignalDesc(pulse, experiment, "Transform/cubicKnotsY"))).getData();
		
		interpX = new CubicInterpolation2D(pLon, pLat, kX);
		interpY = new CubicInterpolation2D(pLon, pLat, kY);
	}
	
	public void saveLinear(GMDSFetcher gmds, String experiment, int pulse){
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "Transform/linearXYtoLL"), linXYtoLL));
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "Transform/linearLLtoXY"), linLLtoXY));				
	}

	public void saveKnots(GMDSFetcher gmds, String experiment, int pulse){
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "Transform/cubicKnotsLon"), pLon));
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "Transform/cubicKnotsLat"), pLat));
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "Transform/cubicKnotsX"), interpX.getfp()));
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "Transform/cubicKnotsY"), interpY.getfp()));				
	}
	
	public void loadLinear(Map<String, Object> metaData){
		linXYtoLL = (double[][])metaData.get("/Transform/linearXYtoLL");
		linLLtoXY = Mat.inv3x3(linXYtoLL);		
	}
	
	public void loadKnots(Map<String, Object> metaData){
		pLon = (double[])metaData.get("/Transform/cubicKnotsLon");
		pLat = (double[])metaData.get("/Transform/cubicKnotsLat");
		
		double kX[][] = (double[][])metaData.get("/Transform/cubicKnotsX");
		double kY[][] = (double[][])metaData.get("/Transform/cubicKnotsY");

		if(pLon == null || pLat == null || kX == null || kY == null){
			logr().warning("No cubic data loaded for FeatureTransformCubic");
			interpX = null;
			interpY = null;
			return;
		}
		
		interpX = new CubicInterpolation2D(getKnotsLon(), getKnotsLat(), kX);
		interpY = new CubicInterpolation2D(getKnotsLon(), getKnotsLat(), kY);
	}	

	@Override
	public double[] xyToLatLon(double X, double Y) {
		return xyToLatLon(X, Y, Double.NaN, Double.NaN);
	}
	
	/** Find XY of a given LL, this is numeric, quite slow and surprisingly bad */
	public double[] xyToLatLon(double X, double Y, double initA, double initB) {
		if(Double.isNaN(initA) || interpX == null){
			initA = linXYtoLL[0][0]*X + linXYtoLL[0][1]*Y + linXYtoLL[0][2];// + RandomManager.instance().nextUniform(-0.05, 0.05);
			initB = linXYtoLL[1][0]*X + linXYtoLL[1][1]*Y + linXYtoLL[1][2];// + RandomManager.instance().nextUniform(-0.05, 0.05);
		
			if(interpX == null){
				return new double[]{ initA, initB };
			}
		}
		
		if(Double.isNaN(interpX.eval(initA, initB)) ||
				Double.isNaN(interpY.eval(initA, initB)))
					return new double[]{ Double.NaN, Double.NaN };
		
		final double targX = X, targY = Y;
		Function costF = new FunctionND() {
			@Override
			public double eval(double ll[]) {
				double X = interpX.eval(ll[0], ll[1]);
				double Y = interpY.eval(ll[0], ll[1]);
				
				return (X - targX)*(X - targX) + (Y - targY)*(Y - targY); 
			}
			public IDomain getDomain() { 
				return new RectangularDomain(
					new double[]{ 0, 0 }, 
					new double[]{ 1, 1 }); 
			}			
		};
		HookeAndJeeves opt = new HookeAndJeeves(costF);
		/*ConjugateGradientDirectionFR cg = new ConjugateGradientDirectionFR();
		CoordinateDescentDirection cd = new CoordinateDescentDirection();
		//GoldenSection ls = new GoldenSection(new MaxIterCondition(500));
		NewtonsMethod1D ls = new NewtonsMethod1D(new MaxIterCondition(50));
				
		LineSearchOptimizer opt = new LineSearchOptimizer(null, cd, ls);//*/
		opt.setObjectiveFunction(costF);
		
		double maxDistSq = 0.0001 * 0.0001;
		
		opt.init(new double[]{ initA, initB });
		
		int n=0;
		while(opt.getCurrentValue() > maxDistSq && n < 1000){ // NaN value (out of grid) will fall out of the loop here too
			opt.refine();
			//double pos[] = opt.getCurrentPos();
			//logr.finest(pos[0] + "\t" + pos[1] + "\t" + opt.getCurrentValue());
			n++;
		}
		
		double pos[] = opt.getCurrentPos();
		if(Double.isNaN(interpX.eval(pos[0], pos[1])) ||
			Double.isNaN(interpY.eval(pos[0], pos[1])))
				return new double[]{ Double.NaN, Double.NaN };
				
		//logr.finest("XY-->LL in " + n + "steps");
		
		return pos;
	}
	
	/** Calculate 3D line of sight unit vectors for all pixels of the image */ 
	public double[][][] calcPixelLOSVectors(PhysicalROIGeometry ccdGeom){
		return calcLOSVectors(ccdGeom.getImgPhysX0(), ccdGeom.getImgPhysY0(), ccdGeom.getImgPhysX1(), ccdGeom.getImgPhysY1(), ccdGeom.getImgPixelW(), ccdGeom.getImgPixelH());		
	}
	
	public double[][][] calcLOSVectors(PhysicalROIGeometry ccdGeom, int width, int height){
		return calcLOSVectors(ccdGeom.getImgPhysX0(), ccdGeom.getImgPhysY0(), ccdGeom.getImgPhysX1(), ccdGeom.getImgPhysY1(), width, height, false );		
	}
	
	/** Calculate 3D line of sight unit vectors (across plasma) for a given grid across the image. */ 
	public double[][][] calcLOSVectors(PhysicalROIGeometry ccdGeom, int width, int height, boolean runningFit){
		return calcLOSVectors(ccdGeom.getImgPhysX0(), ccdGeom.getImgPhysY0(), ccdGeom.getImgPhysX1(), ccdGeom.getImgPhysY1(), width, height, runningFit);		
	}
	
	public double[][][] calcLOSVectors(double x0, double y0, double x1, double y1, int width, int height){
		return calcLOSVectors(x0, y0, x1, y1, width, height, false);
	}
		
	/** Calculate 3D line of sight unit vectors (across plasma) for each of a given grid in physical CCD space */ 
	public double[][][] calcLOSVectors(double x0, double y0, double x1, double y1, int width, int height, boolean runningFit){
		double pixelVectors[][][] = new double[height][width][3];
		
		double dX = (x1 - x0) / width;
		double dY = (y1 - y0) / height;
		double LL[] = new double[]{ Double.NaN, Double.NaN };
		for(int ipy = 0; ipy < height; ipy++){
			for(int ipx = 0; ipx < width; ipx++){
				LL = xyToLatLon(x0 + ipx * dX, 
								y0 + ipy * dY,
								runningFit ? LL[0] : Double.NaN, LL[1]);
				if(!Double.isNaN(LL[0]) && !Double.isNaN(LL[1])){
					
			        pixelVectors[ipy][ipx] = new double[]{
							FastMath.cos(LL[1]) * FastMath.cos(LL[0]),
							FastMath.cos(LL[1]) * FastMath.sin(LL[0]),
							FastMath.sin(LL[1]),
					};

				}else{					
					pixelVectors[ipy][ipx] = new double[]{ Double.NaN, Double.NaN, Double.NaN };
				}
			}	
		}
		
		return pixelVectors;
	}
	
	public void initCubic(int nLon, int nLat) {
		if(pLon == null || nLon != pLon.length || nLat != pLat.length){			
			pLon = Mat.linspace(minLon, maxLon, nLon);
			pLat = Mat.linspace(minLat, maxLat, nLat);
			interpX = new CubicInterpolation2D(pLon, pLat, new double[nLon][nLat]);
			interpY = new CubicInterpolation2D(pLon, pLat, new double[nLon][nLat]);			
		}		
	}
	
	/** Use linear transform of the four corners in XY to calc range of LL */
	public void calcLLRanges(PhysicalROIGeometry ccdGeom){
		//we want the corners of the image, not of the 'full frame'
		
		//and for ranges, start with X matrix of the four corners of the image
		double CA[] = new double[]{
				linXYtoLL[0][0]*ccdGeom.getImgPhysX0() + linXYtoLL[0][1]*ccdGeom.getImgPhysY0() + linXYtoLL[0][2], //top left
				linXYtoLL[0][0]*ccdGeom.getImgPhysX1() + linXYtoLL[0][1]*ccdGeom.getImgPhysY0() + linXYtoLL[0][2], //top right
				linXYtoLL[0][0]*ccdGeom.getImgPhysX0() + linXYtoLL[0][1]*ccdGeom.getImgPhysY1() + linXYtoLL[0][2], //bot left
				linXYtoLL[0][0]*ccdGeom.getImgPhysX1() + linXYtoLL[0][1]*ccdGeom.getImgPhysY1() + linXYtoLL[0][2], //bot right
		};
		double CB[] = new double[]{
				linXYtoLL[1][0]*ccdGeom.getImgPhysX0() + linXYtoLL[1][1]*ccdGeom.getImgPhysY0() + linXYtoLL[1][2], //top left
				linXYtoLL[1][0]*ccdGeom.getImgPhysX1() + linXYtoLL[1][1]*ccdGeom.getImgPhysY0() + linXYtoLL[1][2], //top right
				linXYtoLL[1][0]*ccdGeom.getImgPhysX0() + linXYtoLL[1][1]*ccdGeom.getImgPhysY1() + linXYtoLL[1][2], //bot left
				linXYtoLL[1][0]*ccdGeom.getImgPhysX1() + linXYtoLL[1][1]*ccdGeom.getImgPhysY1() + linXYtoLL[1][2], //bot right
		};
		minLon = Math.min(Math.min(CA[0], CA[1]), Math.min(CA[2], CA[3]));
		maxLon = Math.max(Math.max(CA[0], CA[1]), Math.max(CA[2], CA[3]));
		minLat = Math.min(Math.min(CB[0], CB[1]), Math.min(CB[2], CB[3]));
		maxLat = Math.max(Math.max(CB[0], CB[1]), Math.max(CB[2], CB[3]));
	
		this.pLon = null;
		this.pLat = null;
		this.interpX = null;
		this.interpY = null;
	}
	
	/** (re)Calculate the lat/lon angles from the camera position of each of the fitting points */
	public void calcPointsLatLon() {
		double cameraPos[] = getViewPosition();
		if(cameraPos == null || Double.isNaN(Mat.veclength(cameraPos))){
			throw new IllegalArgumentException("No view position in point data");
		}
		
		synchronized (points) {
			for(TransformPoint p : points){
				double u[] = new double[]{
						p.x - cameraPos[0],
						p.y - cameraPos[1],
						p.z - cameraPos[2],
				};
				u = Mat.normaliseVector(u);
				
				p.lat = FastMath.asin(u[2]);
				p.lon = FastMath.atan2(u[1], u[0]); //might need to do some rotation here if phi becomes discontinuous			
			}
		}
		
	}
	
	/** Use the 3 MODE_FIT_AND_MATRIX points to calculate the approx linear transform between XY and LL and vice versa */
	public void calcLinear() {	
			double linPointsXY[][] = new double[3][3];
			double linPointsLL[][] = new double[3][3];
			
			int nLinearFit = 0;
			for(TransformPoint p : points){				
				if(p.mode == TransformPoint.MODE_FIT_AND_MATRIX){ //use for initial
					linPointsXY[0][nLinearFit] = p.imgX;
					linPointsXY[1][nLinearFit] = p.imgY;
					linPointsXY[2][nLinearFit] = 1;
					linPointsLL[0][nLinearFit] = p.lon;
					linPointsLL[1][nLinearFit] = p.lat;
					linPointsLL[2][nLinearFit] = 1;
					nLinearFit++;
				}
				
				if(nLinearFit >= 3)
					break;
			}
			
			if(nLinearFit < 3){
				throw new IllegalArgumentException("Less than 3 points available for linear transform inversion");					
			}
			
			// calc initial linear trasnform
			double invX[][] = Mat.inv3x3(linPointsXY);
			linXYtoLL = Mat.mul3x3(linPointsLL, invX);
			linLLtoXY = Mat.inv3x3(linXYtoLL);
			
	}

	public boolean isLinearValid() { return (linLLtoXY != null); }
	public boolean isCubicValid() { return (interpX != null); }
	
	@Override
	public boolean isValid() { return isLinearValid(); }
	
	@Override
	public final double[] latlonToXY(double lon, double lat){
		return isCubicValid() 
				? new double[]{ interpX.eval(lon, lat), interpY.eval(lon, lat) } 
				: latlonToXYLinear(lon, lat);
	}
	
	public final double[][] latlonToXY(double lon[], double lat[]){
		return isCubicValid() 
				? new double[][]{ interpX.eval(lon, lat), interpY.eval(lon, lat) } 
				: latlonToXYLinear(lon, lat);
	}
	
	public double[] latlonToXYLinear(double lon, double lat) {
		return new double[]{  
				linLLtoXY[0][0] * lon + linLLtoXY[0][1] * lat + linLLtoXY[0][2],
				linLLtoXY[1][0] * lon + linLLtoXY[1][1] * lat + linLLtoXY[1][2]						
		//	1 = rztoImage[2][0] * pointData[IMSETransform.PD_BI_R] + rztoImage[2][1] * pointData[IMSETransform.PD_BI_Z] + rztoImage[2][2];
			};
	}
	
	public double[][] latlonToXYLinear(double lon[], double lat[]) {
		double xy[][] = new double[2][lon.length];
		for(int i=0; i < lon.length; i++){  
				xy[0][i] = linLLtoXY[0][0] * lon[i] + linLLtoXY[0][1] * lon[i] + linLLtoXY[0][2];
				xy[1][i] = linLLtoXY[1][0] * lat[i] + linLLtoXY[1][1] * lat[i] + linLLtoXY[1][2];						
		}
		return xy;
	}	

	public double[][] getXYtoLLMat() { return linXYtoLL; }
	public double[][] getLLtoXYMat() { return linLLtoXY; }

	public double[][] getKnotValsX() { return (interpX != null) ? interpX.getfp() : null; }; 
	public double[][] getKnotValsY() { return (interpY != null) ? interpY.getfp() : null; }
	
	public void setKnotValsX(double knotValsX[][]){ interpX.setfp(knotValsX); }
	public void setKnotValsY(double knotValsY[][]){ interpY.setfp(knotValsY); }
	
	public double[] getKnotsLon() { return pLon; }
	public double[] getKnotsLat() { return pLat; } 	

	public int getNKnotsLon(){ return pLon.length; }
	public int getNKnotsLat(){ return pLat.length; }
	
	public void initCubicToLinear() {
		
		double pX[][] = new double[pLon.length][pLat.length];
		double pY[][] = new double[pLon.length][pLat.length];
		
		for(int iB = 0; iB < pLat.length; iB++) {
			for(int iA = 0; iA < pLon.length; iA++) {				
				pX[iA][iB] = linLLtoXY[0][0]*pLon[iA] + linLLtoXY[0][1]*pLat[iB] + linLLtoXY[0][2];
				pY[iA][iB] = linLLtoXY[1][0]*pLon[iA] + linLLtoXY[1][1]*pLat[iB] + linLLtoXY[1][2];				
			}				
		}
		
		interpX.setfp(pX);
		interpY.setfp(pY);
		
	}

	public void invalidateCubic() { interpX = null; interpY = null; }

	
}
