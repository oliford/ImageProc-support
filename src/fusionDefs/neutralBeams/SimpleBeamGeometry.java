package fusionDefs.neutralBeams;

import java.util.LinkedList;

import algorithmrepository.Algorithms;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Definition of neutral beam geometry, drawing etc 
 * For now just the fixed coords from minerva-land but later we'll
 * bring that geom stuff here
 * 
 * */
public abstract class SimpleBeamGeometry  {

	/** The beam indices actually match the AUG naming convention (except for the 1-based indexing)
	 * Q means 'Quelle' ('source' in German) */
	public static final int BEAM_BOXAVG = -1;
	public static final int BEAM_Q1 = 0;
	public static final int BEAM_Q2 = 1;
	public static final int BEAM_Q3 = 2;	
	public static final int BEAM_Q4 = 3;
	public static final int BEAM_Q5 = 4;
	public static final int BEAM_Q6 = 5;
	public static final int BEAM_Q7 = 6;
	public static final int BEAM_Q8 = 7;
	
		
	public final double[] start(int beamIdx){ return startAll()[beamIdx]; }
	public final double[] uVec(int beamIdx){ return uVecAll()[beamIdx];  }
	public final int nBeams(){ return startAll().length; }
	
	public abstract double[][] startAll();
	public abstract double[][] uVecAll();
	public abstract double[] getVoltageAll();
	
	public abstract double[] startBox(int boxIdx);
	public abstract double[] uVecBox(int boxIdx);
	
	protected double beamWidth;
	protected double plasmaR0;
	protected double plasmaR1;
	protected double sourceR;
	
	public double beamWidth(){ return beamWidth; }
	public double plasmaR0(){ return plasmaR0; }
	public double plasmaR1(){ return plasmaR1; }
	public double sourceR(){ return sourceR; }
		
	public double getLOfBeamAxisAtR(int beamIdx, double R) {
		double t[] = Algorithms.cylinderLineIntersection(start(beamIdx), uVec(beamIdx), new double[]{0,0,0}, new double[]{0,0,1}, R*R);
		//if(t[0] >= 0 && t[1] < 0) return t[0];
		//if(t[0] < 0 && t[1] >= 0) return t[1];
		return (t.length == 0) ? Double.NaN : Math.min(t[0],t[1]);
	}
	
	public double[] getPosOfBeamAxisAtR(int beamIdx, double R){		
		return Mat.add(start(beamIdx), Mat.mul(uVec(beamIdx), getLOfBeamAxisAtR(beamIdx, R)));
	}
	
	public double[] getPosOfBoxAxisAtR(int boxIdx, double R){		
		double t[] = Algorithms.cylinderLineIntersection(startBox(boxIdx), uVecBox(boxIdx), new double[]{0,0,0}, new double[]{0,0,1}, R*R);
		return Mat.add(startBox(boxIdx), Mat.mul(uVecBox(boxIdx), Math.min(t[0],t[1])));
	}
		
	protected final double[] avgVec(double vecs[][], int idxs[]){
		double vec[] = new double[3];
		for(int i=0; i < idxs.length; i++)
			for(int k=0; k < 3; k++)
				vec[k] += vecs[idxs[i]][k] / idxs.length;
		return vec;
	}
	
}
