package fusionDefs.sensorInfo;

import java.util.Map;
import java.util.Map.Entry;

import fusionDefs.sensorInfo.SensorInfoService.SensorParser;
import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;

public class SensorInfo_AndorV2 implements SensorParser {
	
	@Override
	public SensorInfo attemptGet(MetaDataGetter src) {		
		//try to find the camera metadata		
		
		// Andor CMOS cameras should always be under 'AndorCam'.
		try { 
			Double ccdWidth = src.get("AndorV2/config/exposureTime", Double.class);
			if(ccdWidth == null) // not an AndorV2
				return null;
		}catch(RuntimeException err) {			
			return null; //No AndorV2 metadata, ignore
		}

        
		SensorInfo camInfo = new SensorInfo();
		camInfo.zeroLevel = Double.NaN;
		camInfo.photoelectronsPerADCU = Double.NaN;
		camInfo.readNoise = Double.NaN;
			
		//based on camera ROIs
		camInfo.sensorPixelOfImageLeft = -1;
		camInfo.sensorPixelOfImageTop = -1;
		camInfo.sensorBinningX = -1;
		camInfo.sensorBinningY = -1;
		
		camInfo.transposedImage = false; //camera doesn't support it	
		
		camInfo.exposureTime = src.get("/AndorV2/config/exposureTime", Double.class);
		camInfo.frameTime = src.get("/AndorV2/config/accumulationCycleTime", Double.class);
		camInfo.cameraName = "AndorV2-" + src.get("/AndorV2/config/headModel", String.class) + "-" + src.get("/AndorV2/config/cameraSerialNumber", String.class);
		camInfo.modeString = camInfo.cameraName + "...?";
		
		return camInfo;
		
	}


}
