package fusionDefs.sensorInfo;

import java.util.Map;
import java.util.Map.Entry;

import fusionDefs.sensorInfo.SensorInfoService.SensorParser;
import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;

public class SensorInfo_AndorCMOS implements SensorParser {
	
	@Override
	public SensorInfo attemptGet(MetaDataGetter src) {		
		//try to find the camera metadata		
		
		// Andor CMOS cameras should always be under 'AndorCam'.
		try { 
			Double ccdWidth = src.get("AndorCam/config/SensorWidth", Double.class);
			if(ccdWidth == null) // not a PCO
				return null;
		}catch(RuntimeException err) {			
			return null; //No AndorCam metadata, ignore
		}
		
		SensorInfo camInfo = new SensorInfo();
		
		String gainControl = src.get("/AndorCam/config/SimplePreAmpGainControl", String.class);
		String shutterMode = src.get("/AndorCam/config/ElectronicShutteringMode", String.class);
		//camInfo.zeroLevel = src.get("/AndorCam/config/BaselineLevel", Double.class);
		camInfo.zeroLevel = 100; //this just seems to /be/ 
					
		if(gainControl != null && shutterMode != null){
			if(shutterMode.startsWith("Rolling")){
				if(gainControl.startsWith("12-bit (high well")){
					camInfo.photoelectronsPerADCU = 20.0;
				}else if(gainControl.startsWith("12-bit (low noise")){
					camInfo.photoelectronsPerADCU = 0.6;
				}else if(gainControl.startsWith("16-bit (low noise & high well")){
					camInfo.photoelectronsPerADCU = 0.6;
				}
				camInfo.readNoise = 1.5 // in electrons, varies for readout rate and anyway isn't really a sigma
								/ camInfo.photoelectronsPerADCU; //convert to ADCU
				
			}else if(shutterMode.startsWith("Global")){
				if(gainControl.startsWith("12-bit (high well")){
					camInfo.photoelectronsPerADCU = 20.0;
				}else if(gainControl.startsWith("12-bit (low noise")){
					camInfo.photoelectronsPerADCU = 1.8;
				}else if(gainControl.startsWith("16-bit (low noise & high well")){
					camInfo.photoelectronsPerADCU = 1.8;
				}
				camInfo.readNoise = 3.0 // in electrons, varies for readout rate and anyway isn't really a sigma
						/ camInfo.photoelectronsPerADCU;  //convert to ADCU
			}
		}

		if(Double.isNaN(camInfo.photoelectronsPerADCU))
			throw new IllegalArgumentException("AndorCam: Unrecognised shutter/gain modes");
		
		//camera AOI (which is 1-based!!)
		camInfo.sensorPixelOfImageLeft = src.get("/AndorCam/config/AOILeft", Integer.class) - 1;
		camInfo.sensorPixelOfImageTop = src.get("/AndorCam/config/AOITop", Integer.class) - 1;
		String aoiBinning = src.get("/AndorCam/config/AOIBinning", String.class);
		String parts[] = aoiBinning.split("x");
		
		camInfo.sensorBinningX = Integer.parseInt(parts[0]);
		camInfo.sensorBinningY = Integer.parseInt(parts[1]);
		
		camInfo.transposedImage = false; //camera doesn't support it	
		
		
		camInfo.exposureTime = src.get("/AndorCam/config/ExposureTime", Double.class);
		camInfo.frameTime = 1.0 / src.get("/AndorCam/config/FrameRate", Double.class);
		camInfo.cameraName = "AndorCMOS-" + src.get("/AndorCam/config/CameraModel", String.class);
		camInfo.modeString = camInfo.cameraName + ", " + shutterMode + " shutter, " + gainControl;
		
		
		return camInfo;
		
	}


}
