package fusionDefs.sensorInfo;

import java.util.Map;
import java.util.Map.Entry;

import fusionDefs.sensorInfo.SensorInfoService.SensorParser;
import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;

/** Sensor mode info from metadata for OceanOptics spectrometers driven by the OmniDriver source module */ 
public class SensorInfo_OceanOptics implements SensorParser {
	
	@Override
	public SensorInfo attemptGet(MetaDataGetter src) {		
		//try to find the camera metadata

		String camName;
		try {
			camName = (String) src.get("OmniDriver/config/name", String.class);
			if(camName == null) // not a PCO
				return null;
		}catch(RuntimeException err) {			
			return null; //No OmniDriver metadata, ignore
		}
		
		SensorInfo camInfo = new SensorInfo();
		camInfo.cameraName = "OceanOptics " + camName;
						
		//we seem to have very little information from these
		camInfo.exposureTime = (Long)src.get("OmniDriver/config/integrationTimeUS", Long.class) / 1e6;
		
		camInfo.zeroLevel = Double.NaN;
		camInfo.photoelectronsPerADCU = Double.NaN;
		camInfo.frameTime = Double.NaN;
		camInfo.modeString = camInfo.cameraName;		
		
		return camInfo;
		
	}


}
