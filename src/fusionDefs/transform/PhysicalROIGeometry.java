package fusionDefs.transform;


import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import mds.GMDSFetcher;
import signals.gmds.GMDSSignal;
import signals.gmds.GMDSSupport;
import descriptors.gmds.GMDSSignalDesc;

/** Determines and holds information about the CCD physical size and pixels as well as the selected portion of it (AOI/ROI) for a particular image (set). 
 *  
 * This gives the translation from physical image size (in m) to image coordinates in pixels.
 * This is based on the camera used, and the ROI selected, so can be different for each image. 
 * 
 * This is typically for CNMOS cameras and can define only a rectangular area with regular x/y binning.
 * 
 * This is a bit fiddly because it can be saved/loaded directly to/from GMDS signals here, but is also used by IMSEProc, which has it's own internal
 *   metadata storage. 
 * 
 * @author oliford
 */
public class PhysicalROIGeometry {
	
	
	/** Identifier of camera/CCD type */
	protected String cameraName;
	
	/** Physical pixel spacing */
	protected double physPixelWidth, physPixelHeight;
	
	/** Number of pixels on on full CCD */
	protected int ccdWidthPixels, ccdHeightPixels;
		
	/** ROI top-left position on CCD in CCD pixels */
	protected int roiX0, roiY0;
	
	/** ROI size in pixels of the output image */
	protected int roiW, roiH;
	
	/** Binning - CCD pixels per ROI pixel in each direction */  
	protected int roiDX, roiDY;
	
	/** Effective binning/step due to FFT cropping. To multiply roiDX/roiDY */
	protected double fftDX, fftDY;
	
	/** Metadata save/loaded for this class */
	public static final String[] metadataNames = {
		"/CCDGeom/cameraName",
		"/CCDGeom/physPixelWidth",
		"/CCDGeom/physPixelHeight",
		"/CCDGeom/ccdWidthPixels",
		"/CCDGeom/ccdHeightPixels",
		"/CCDGeom/roiX0",
		"/CCDGeom/roiY0",
		"/CCDGeom/roiW",
		"/CCDGeom/roiH",
		"/CCDGeom/roiDX",	
		"/CCDGeom/roiDY",
		"/CCDGeom/fftDX",	
		"/CCDGeom/fftDY",	
	};
	
	/** Metadata needed by setFromCameraMetadata() */
	public static final String[] cameraMetadataNames = {				
				"/AndorCam/config/PixelWidth",
				"/AndorCam/config/PixelHeight",
				"/AndorCam/config/SensorWidth",
				"/AndorCam/config/SensorHeight",
				"/AndorCam/config/AOIWidth",
				"/AndorCam/config/AOIHeight",
				"/AndorCam/config/AOILeft",
				"/AndorCam/config/AOITop",
				"/AndorCam/config/AOIBinning",
				"/PCOCam/config/cameraInfo",
				"/PCOCam/config/binningX",
				"/PCOCam/config/binningY",
				"/PCOCam/config/roiX0",
				"/PCOCam/config/roiX1",
				"/PCOCam/config/roiY0",
				"/PCOCam/config/roiY1",
				"/PCOCam/config/sensorWidth",
				"/PCOCam/config/sendorHeight",
				"/PCOCam/config/imageWidth",
				"/PCOCam/config/imageHeight",				
				"/Sensicam/config/roiX0",
				"/Sensicam/config.roiY0",
				"/Sensicam/config/binX",
				"/Sensicam/config.binY",
				"/Sensicam/param/ccdheight",
				"/Sensicam/param/ccdwidth",
				"/FFT/fftWidth",
				"/FFT/fftHeight",
		};
	
	public PhysicalROIGeometry(GMDSFetcher gmds, String experiment, int pulse) {
		loadFromSignals(gmds, experiment, pulse);
	}
	
	public PhysicalROIGeometry() {
		cameraName = "INVALID";
		physPixelWidth = Double.NaN;
		physPixelHeight = Double.NaN;
		ccdWidthPixels = -1;
		ccdHeightPixels = -1;
		roiX0 = -1;
		roiY0 = -1;
		roiW = -1;
		roiH = -1;
		roiDX = -1;
		roiDY = -1;
		fftDX = Double.NaN;
		fftDY = Double.NaN;
	}
	
	/** Save state directly to GMDS signals */
	public void saveToSignal(GMDSFetcher gmds, String experiment, int pulse){
		for(Entry<String, Object> entry : toMap().entrySet()){
			
			GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, entry.getKey());
			Object contents = entry.getValue();
			Object arr = Array.newInstance(contents.getClass(), 1);
			Array.set(arr, 0, contents);		
			GMDSSignal sig = new GMDSSignal(sigDesc, arr);
			gmds.writeToCache(sig);			
		}
	}
	
	/** Return state as HashMap of metadata */
	public Map<String, Object> toMap(){
		HashMap<String, Object> metaData = new HashMap<String, Object>();
				
		metaData.put("/CCDGeom/cameraName", cameraName);
		metaData.put("/CCDGeom/physPixelWidth", physPixelWidth);
		metaData.put("/CCDGeom/physPixelHeight", physPixelHeight);
		metaData.put("/CCDGeom/ccdWidthPixels", ccdWidthPixels);
		metaData.put("/CCDGeom/ccdHeightPixels", ccdHeightPixels);
		metaData.put("/CCDGeom/roiX0", roiX0);
		metaData.put("/CCDGeom/roiY0", roiY0);
		metaData.put("/CCDGeom/roiW", roiW);
		metaData.put("/CCDGeom/roiH", roiH);
		metaData.put("/CCDGeom/roiDX", roiDX);
		metaData.put("/CCDGeom/roiDY", roiDY);
		metaData.put("/CCDGeom/fftDX", fftDX);
		metaData.put("/CCDGeom/fftDY", fftDY);
		
		
		return metaData;
	}
	
	/** Load state from GMDS signals */
	public void loadFromSignals(GMDSFetcher gmds, String experiment, int pulse){

		Map<String, Object> metaData = GMDSSupport.mapFromSignals(gmds, experiment, pulse, "", metadataNames);
		fromMap(metaData);
	}
	
	/** Sets the state from the metadata map */
	public void fromMap(Map<String, Object> metaData){
	
		cameraName = (String) metaData.get("/CCDGeom/cameraName");
		physPixelWidth = (Double) metaData.get("/CCDGeom/physPixelWidth");
		physPixelHeight = (Double) metaData.get("/CCDGeom/physPixelHeight");
		ccdWidthPixels = (Integer) metaData.get("/CCDGeom/ccdWidthPixels");
		ccdHeightPixels = (Integer) metaData.get("/CCDGeom/ccdHeightPixels");
		roiX0 = (Integer) metaData.get("/CCDGeom/roiX0");
		roiY0 = (Integer) metaData.get("/CCDGeom/roiY0");
		roiW = (Integer) metaData.get("/CCDGeom/roiW");
		roiH = (Integer) metaData.get("/CCDGeom/roiH");
		roiDX = (Integer) metaData.get("/CCDGeom/roiDX");
		roiDY = (Integer) metaData.get("/CCDGeom/roiDY");
		fftDX = (Integer) metaData.get("/CCDGeom/fftDX");
		fftDY = (Integer) metaData.get("/CCDGeom/fftDY");
	}

	public final double getImgPhysX0() { return -(ccdWidthPixels*physPixelWidth)/2 + roiX0 * physPixelWidth; }
	public final double getImgPhysX1() { return -(ccdWidthPixels*physPixelWidth)/2 + (roiX0 + roiW * roiDX*fftDX) * physPixelWidth; }
	
	public final double getImgPhysY0() { return -(ccdHeightPixels*physPixelHeight)/2 + roiY0 * physPixelHeight; }
	public final double getImgPhysY1() { return -(ccdHeightPixels*physPixelHeight)/2 + (roiY0 + roiH * roiDY*fftDY) * physPixelHeight; }
	
	public final double getImgPhysW() { return roiW * roiDX*fftDX * physPixelWidth; }
	public final double getImgPhysH() { return roiH * roiDY*fftDY * physPixelHeight; }
	
	public final double pixelToPhysX(int pixelX){ return pixelToPhysX((double)pixelX); }
	public final double pixelToPhysY(int pixelY){ return pixelToPhysY((double)pixelY); }
	
	public final double pixelToPhysX(double pixelX){ return -(ccdWidthPixels*physPixelWidth)/2 + roiX0 * physPixelWidth + pixelX * (roiDX*fftDX) * physPixelWidth; }
	public final double pixelToPhysY(double pixelY){ return -(ccdHeightPixels*physPixelHeight)/2 + roiY0 * physPixelHeight + pixelY * (roiDY*fftDY) * physPixelHeight; }
	
	public final int physToPixelX(double physX){ return (int)((physX + (ccdWidthPixels*physPixelWidth)/2 - roiX0 * physPixelWidth) / (roiDX*fftDX) / physPixelWidth);	}
	public final int physToPixelY(double physY){ return (int)((physY + (ccdHeightPixels*physPixelHeight)/2 - roiY0 * physPixelHeight) / (roiDY*fftDY) / physPixelHeight);	}
	
	/** Convert full frame pixel location to image pixel locations */
	public final int sensorToImageX(int sensorX){ return (int)((sensorX - roiX0) / (roiDX*fftDX)); }
	public final int sensorToImageY(int sensorY){ return (int)((sensorY - roiY0) / (roiDY*fftDY)); }
	
	/** Convert full frame pixel location to image pixel locations (subpixel) */
	public final double sensorToImageX(double sensorX){ return ((sensorX - roiX0) / (roiDX*fftDX)); }
	public final double sensorToImageY(double sensorY){ return ((sensorY - roiY0) / (roiDY*fftDY)); }

	/** Convert image pixel locations to full frame pixel locations*/
	public final int imageToSensorX(int pixelX){ return (int)(pixelX * (roiDX*fftDX) + roiX0); }
	public final int imageToSensorY(int pixelY){ return (int)(pixelY * (roiDY*fftDY) + roiY0); }

	/** Convert image pixel locations to full frame pixel locations (subpixel) */
	public final double imageToSensorX(double pixelX){ return (int)(pixelX * (roiDX*fftDX) + roiX0); }
	public final double imageToSensorY(double pixelY){ return (int)(pixelY * (roiDY*fftDY) + roiY0); }

	/** Width of ROI (final image) in pixels */
	public final int getImgPixelW() { return roiW; }
	
	/** Height of ROI (final image) in pixels */
	public final int getImgPixelH() { return roiH; }
	
	/**Sets the width and heigh of the ROI (image) */
	public final void setImageSize(int width, int height) {
		this.roiW = width;
		this.roiH = height;
	}
	
	public void setFromCameraMetadata(GMDSFetcher gmds, String experiment, int pulse, String subPath) {		
		setFromCameraMetadata(GMDSSupport.mapFromSignals(gmds, experiment, pulse, subPath, cameraMetadataNames));
	}

	public void setFromCameraMetadata(Map<String, Object> cameraMetaDataStupid) {
		HashMap<String, Object> cameraMetaData = new HashMap<String, Object>();
		for(Entry<String, Object> stupid : cameraMetaDataStupid.entrySet()){
			String keyName = stupid.getKey();
			if(!keyName.startsWith("/")){
				keyName = "/" + keyName;
			}
			cameraMetaData.put(keyName, stupid.getValue());
		}
		
		
		if(cameraMetaData.get("/AndorCam/config/AOIWidth") != null){
			cameraName = "AndorZyla";
			physPixelWidth = getSingleDbl(cameraMetaData, "AndorCam/config/PixelWidth") * 1e-6;
			physPixelHeight = getSingleDbl(cameraMetaData, "AndorCam/config/PixelHeight") * 1e-6;
			ccdWidthPixels = getSingleInt(cameraMetaData, "AndorCam/config/SensorWidth");
			ccdHeightPixels = getSingleInt(cameraMetaData, "AndorCam/config/SensorHeight");
						
			roiX0 = getSingleInt(cameraMetaData, "AndorCam/config/AOILeft") - 1;
			roiY0 = getSingleInt(cameraMetaData, "AndorCam/config/AOITop") - 1;
			roiW = getSingleInt(cameraMetaData, "AndorCam/config/AOIWidth");
			roiH = getSingleInt(cameraMetaData, "AndorCam/config/AOIHeight");
			
			Object o = getSingleObj(cameraMetaData, "/AndorCam/config/AOIBinning");
			if(o == null) o = "1x1";
			else if(o.getClass().isArray()) o = Array.get(o, 0);
			roiDX = Algorithms.mustParseInt(
						((String)o).substring(0, 1));
			roiDY = roiDX;
			
			
		}else if(cameraMetaData.get("/PCOCam/config/cameraInfo") != null &&
				((String)cameraMetaData.get("/PCOCam/config/cameraInfo")).startsWith("pco.edge 5.5")){
			
			cameraName = "PCO Edge 5.5";
			physPixelWidth = 9.9e-6; //getSingleDbl(cameraMetaData, "AndorCam/config/PixelWidth") * 1e-6;
			physPixelHeight = 9.9e-6; //getSingleDbl(cameraMetaData, "AndorCam/config/PixelHeight") * 1e-6;
			ccdWidthPixels = getSingleInt(cameraMetaData, "PCOCam/config/sensorWidth");
			//ccdHeightPixels = getSingleInt(cameraMetaData, "PCOCam/config/sensorHeight");
			ccdHeightPixels = 2160;
				
			roiX0 = getSingleInt(cameraMetaData, "PCOCam/config/roiX0") - 1;
			roiY0 = getSingleInt(cameraMetaData, "PCOCam/config/roiY0") - 1;
			roiW = getSingleInt(cameraMetaData, "PCOCam/config/imageWidth");
			roiH = getSingleInt(cameraMetaData, "PCOCam/config/imageHeight");
			roiDX = getSingleInt(cameraMetaData, "PCOCam/config/binningX");
			roiDY = getSingleInt(cameraMetaData, "PCOCam/config/binningY");
	
		}else if(getSingleObj(cameraMetaData, "Sensicam/config/roiX0") != null){ 
			ccdWidthPixels = getSingleInt(cameraMetaData, "Sensicam/param/ccdwidth");
			ccdHeightPixels = getSingleInt(cameraMetaData, "Sensicam/param/ccdheight");
			if(ccdWidthPixels == 640){
				cameraName = "Sensicam VGA";	
				physPixelWidth = 9.9e-6;
				physPixelHeight = 9.9e-6;
			}else if(ccdWidthPixels == 1280){
				cameraName = "Sensicam SVGA";	
				physPixelWidth = 6.7e-6;
				physPixelHeight = 6.7e-6;				
			
			}else{
				cameraName = "Sensicam (assumed VGA)";
				physPixelWidth = 9.9e-6;
				physPixelHeight = 9.9e-6;
				System.err.println("PhysicalROIGeometry: WARNING: Unrecognised Sensicam, assuming pixel size");
			}
			
			roiX0 = getSingleInt(cameraMetaData, "Sensicam/config/roiX0");
			roiY0 = getSingleInt(cameraMetaData, cameraMetaData.containsKey("Sensicam/config.roiY0") ? "Sensicam/config.roiY0" : "Sensicam/config/roiY0");
			roiDX = getSingleInt(cameraMetaData, "Sensicam/config/binX");
			roiDY = getSingleInt(cameraMetaData, cameraMetaData.containsKey("Sensicam/config.binY") ? "Sensicam/config.binY" : "Sensicam/config/binY");
			
		}else{ //probably the sensicam in default mode
			System.err.println("PhysicalROIGeometry: WARNING: No CCD Metadata.");
			physPixelWidth = 0.001; //just use  pixels as mm
			physPixelHeight = 0.001;
			ccdWidthPixels = -1; //1m ccd!!
			ccdHeightPixels = -1;			
			roiX0 = 0;
			roiY0 = 0;
			roiDX = 1;
			roiDY = 1;
		}
		
		//If the image was cropped in frequency space by FFTProcessor, the inverse transform will now have a different DX,DY
		Number fftW = (Number)getSingleObj(cameraMetaData, "FFT/fftWidth");
		Number fftH = (Number)getSingleObj(cameraMetaData, "FFT/fftHeight");
		
		if(fftW != null && fftH != null){			
			fftDX = (double)roiW / ((Number)fftW).intValue();
			fftDY = (double)roiH / ((Number)fftH).intValue();
		}else{
			fftDX = 1;
			fftDY = 1;
		}
	}
	
	public int getSingleInt(Map<String, Object> map, String id){
		Object o = getSingleObj(map, id);
		return ((Number)o).intValue();			
	}

	public double getSingleDbl(Map<String, Object> map, String id){
		Object o = getSingleObj(map, id);
		return (o == null) ? null : ((Number)o).doubleValue();			
	}

	public Object getSingleObj(Map<String, Object> map, String id){
		Object val = map.get(id);
		if(val == null)
			val = map.get("/" + id);
		if(val == null)
			return null;
		//If the thing is a 1 length array, return the thing in the array element 0
		if(val.getClass().isArray()){
			int len = Array.getLength(val);
			if(len == 0)
				return null;
			if(len == 1)
				return Array.get(val, 0);
			else
				throw new RuntimeException("Wanted a single element for metaData '"+id+"', but it is an array length " + len);
		}
		return val;
	}
}
