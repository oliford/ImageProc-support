package fusionDefs.sensorInfo;

import java.util.Map;
import java.util.Map.Entry;

import fusionDefs.sensorInfo.SensorInfoService.SensorParser;
import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;

public class SensorInfo_PCOEdge implements SensorParser {
	
	@Override
	public SensorInfo attemptGet(MetaDataGetter src) {		
		//try to find the camera metadata

		try { 
			Integer ccdWidth = (Integer) src.get("PCOCam/config/sensorWidth", Integer.class);
			if(ccdWidth == null) // not a PCO
				return null;
		}catch(RuntimeException err) {			
			return null; //No PCOCam metadata, ignore
		}
		
		SensorInfo camInfo = new SensorInfo();
				
		//camInfo.zeroLevel = (Double)src.get("/AndorCam/config/BaselineLevel", Double.class);
		camInfo.zeroLevel = 100; //just seems to be

		//from the PCO we seem to get this done for us
		//but the datasheet gives 0.46 e- per ADCU
		camInfo.photoelectronsPerADCU = (Double)src.get("PCOCam/config/photoelectronsPerCount", Double.class);
		
		//if(global shutter)
		camInfo.readNoise = 2.5 / camInfo.photoelectronsPerADCU; //from datasheet
		//else(rolling shutter)
		//camInfo.readNoise = 1.5 / camInfo.photoelectronsPerADCU;
		
		if((Long)src.get("PCOCam/config/exp_timebase", Long.class) != 1)
			throw new RuntimeException("PCO timebase unit is unusual. Not implemented.");
			
		camInfo.exposureTime = (Long)src.get("PCOCam/config/exp_time", Long.class) / 1e6; //in us
		camInfo.frameTime = (Double)src.get("PCOCam/config/frameTimeMS", Double.class) / 1e3;
		camInfo.cameraName = "PCOCam-" + (String)src.get("/PCOCam/config/cameraInfo", String.class);
		camInfo.modeString = camInfo.cameraName;		

		camInfo.sensorPixelOfImageLeft = src.get("/PCOCam/config/roiX0", Integer.class) - 1;
		camInfo.sensorPixelOfImageTop = src.get("/PCOCam/config/roiY0", Integer.class) - 1;

		camInfo.sensorBinningX = src.get("/PCOCam/config/binningX", Integer.class);
		camInfo.sensorBinningY = src.get("/PCOCam/config/binningY", Integer.class);
		
		return camInfo;
		
	}


}
