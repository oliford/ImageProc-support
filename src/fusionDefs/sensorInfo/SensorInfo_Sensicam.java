package fusionDefs.sensorInfo;

import java.util.Map;
import java.util.Map.Entry;

import fusionDefs.sensorInfo.SensorInfoService.SensorParser;
import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;

public class SensorInfo_Sensicam implements SensorParser {
	
	@Override
	public SensorInfo attemptGet(MetaDataGetter src) {		
		
		//see if it's definitely the sensicam
		Integer sensicamGain;
		try {
			sensicamGain = (Integer)src.get("/Sensicam/config/gainMode", Integer.class);		
			if(sensicamGain == null) //not a sensicam
				return null;
		}catch(RuntimeException err) {			
			return null; //No sensicam metadata, ignore
		}
		
		int imageWidth = -1; // : Erm... don't know how to get the image width in here
		if(true) throw new RuntimeException("Sensicam not implemented!!!!");
		
		SensorInfo camInfo = new SensorInfo();
					
		if((imageWidth % 320) == 0){ //probably Sensicam VGA
			camInfo.photoelectronsPerADCU = 7.5;
			camInfo.readNoise = 14.0 / camInfo.photoelectronsPerADCU;
			camInfo.zeroLevel = 55.0; //roughly
			if(sensicamGain == null)
				System.err.println("WARNING: SensorInfo_Sensicam: Not entirely sure that this was a sensicam, but it looks like Sensicam VGA, so assuming 7.5 e- per ADU");
			
			camInfo.cameraName = "Sensicam VGA";
		}else if((imageWidth % 344) == 0){ // Sensicam QE is 1376x, so we'll allow down to 1/4
			if(sensicamGain == null){
				System.err.println("No gain mode for Sensicam QE, no error calc");
				camInfo.photoelectronsPerADCU = Double.NaN;
			}else{ 
				if(sensicamGain == 0){
					camInfo.photoelectronsPerADCU = 4.0;						
				}else{
					camInfo.photoelectronsPerADCU = 2.0;
				}
				camInfo.readNoise = 5.0 / camInfo.photoelectronsPerADCU;
				System.err.println("WARNING: SensorInfo_Sensicam: Looks like Sensicam QE with gain mode '"+sensicamGain +"', so assuming " 
				+ camInfo.photoelectronsPerADCU+" e- per ADU. **** CHECK THIS!!! ****");
			}	
			
			camInfo.cameraName = "Sensicam QE";
		}else{
			throw new RuntimeException("SensorInfo_Sensicam: Camera is a PCO Sensicam, but the image size is indicates an unrecognised model.");				
		}
		
		camInfo.exposureTime = Double.NaN; //TODO: Add up from delay_time, bel_time, coc_time etc
		camInfo.frameTime = Double.NaN;
		
		return camInfo;
		
	}

}
